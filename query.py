import json
import nltk


class Query:
    def __init__(self):
        self.setup_questions()

        # Load the KB file

    def setup_questions(self):
        """
			load knowledge base from data/qa.json
		"""
        with open("data/qa.json") as fin:
            self.questions = json.load(fin)

    def clean_sentences(self, question):
        """
			remove is, are, was, were from a query asked by the user

			:param question: question asked by user to query knowledge base
			: type question: str.
			:returns: str -- returns the question are removing the words in the list_words
		"""
        list_words = ["is ", "are ", "was ", "were "]
        for i in list_words:
            question = question.replace(i, "")
        return question

    def match_question(self, question):
        """
		iterate over all question and find the question with smallest distance to 'question'

		:param question: the question to look-up in the knowledge base.
		:type question: str. 
		:returns: int -- the closest question's id in the knowledge base
		"""
        num_questions = len(self.questions)
        min_dist = float("inf")
        min_id = -1
        question = question.lower()
        question = self.clean_sentences(question)
        for i in range(num_questions):
            curr_q = self.questions[i]["question"]
            curr_q = self.clean_sentences(curr_q)

            dist = nltk.jaccard_distance(set(curr_q.lower()), set(question.lower()))

            if dist < min_dist:
                min_id = i
                min_dist = dist

        if min_dist > 0.1:
            return -2
        else:
            return min_id

    def get_answer(self, q_id):
        """
			Retrieve the answer given the question id from the KB
			:param q_id: the question id to look-up in the KB. 
			:type q_id: int.
			:returns: str -- the answer given the q_id. 
		"""
        return self.questions[q_id]["answer"]

    def get_question(self, q_id):
        """
			Retrieve the question given the question id from the KB
			:param q_id: the question id to look-up in the KB. 
			:type q_id: int.
			:returns: str -- the question given the q_id. 
		"""
        return self.questions[q_id]["question"]


if __name__ == "__main__":
    query = Query()

    # Start Test
    my_quest = "What's Bilbo Baggins book called?"
    j = query.match_question(my_quest)
    print("matched q: %s real q: %s" % (query.get_question(j), my_quest))
    print(query.get_answer(j))
    print("\n")

    my_quest = "What species are the Orcs?"
    j = query.match_question(my_quest)
    print("matched q: %s real q: %s" % (query.get_question(j), my_quest))
    print(query.get_answer(j))
    print("\n")

    my_quest = "Where do Saruman and Gandalf meet?"
    j = query.match_question(my_quest)
    print("matched q: %s real q: %s" % (query.get_question(j), my_quest))
    print(query.get_answer(j))
    # End Test
